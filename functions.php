<?php
/**===================================
 * MENU
 ===================================*/
/**
 * Agregar item al menu
 */
if (!function_exists('wixsms_suscribe_menu')) {
	function wixsms_suscribe_menu() {
		add_menu_page('WixSms', 'WixSms', 'manage_options', 'wixsms' );
		add_submenu_page( 'wixsms', 'Configuración', 'Configuración', 'manage_options', 'wixsms_suscribe_admin', 'wixsms_suscribe_admin' );
		add_submenu_page( 'wixsms', 'Suscriptores', 'Suscriptores', 'manage_options', 'wixsms_suscribe_suscriptores', 'wixsms_suscribe_suscriptores' );
    //add_submenu_page( 'sorteo', 'Exportar', 'Exportar', 'manage_options', 'viewImportarSuscriptores', 'viewImportarSuscriptores' );
		remove_submenu_page("wixsms", "wixsms");
		//vaciarSuscriptores();
	}
}

/**
 * Admin bar
 */
if (!function_exists('wixsms_registrar_admin_bar')) {
	function wixsms_registrar_admin_bar( $wp_admin_bar ) {
		$wp_admin_bar->add_menu(array(
			'id' => 'wixsms_suscribe_opciones_bar',
			'parent' => 'appearance',
			'title' => __( 'WIXSMS Suscribe', 'wixsms-xtheme' ),
			'href' => admin_url( 'themes.php?page=wixsms_suscribe_admin' ),
			'meta' 	=> array(),
			1000
		));
	}
}

/**===================================
 * VISTAS
 ===================================*/


/**
 * Vista de administracion
 */
if (!function_exists('wixsms_suscribe_admin')) {
	function wixsms_suscribe_admin() {
		require_once dirname(__FILE__) . '/front-end.php';
	}
}

/**
 * Vista de lista de suscriptores
 */
if (!function_exists('wixsms_suscribe_suscriptores')) {
	function wixsms_suscribe_suscriptores() {


		if(isset($_GET['format']) && $_GET['format']!=''){
			viewImportarSuscriptores();
		}
		else{
			if(isset($_POST['vaciarLista'])){
				vaciarSuscriptores();
			}
		else {
			if(isset($_POST['enviarEmail'])){
				enviarEmailPendientes();
			}
		}
		$suscriptores = get_suscriptores();
    $todos_suscriptores=get_todos_suscriptores();
		$count_suscriptores = count($todos_suscriptores);
		require_once dirname(__FILE__) . '/views/wixsms_suscribe_suscriptores.php';
		}
	}
}

if (!function_exists('viewImportarSuscriptores')) {
	function viewImportarSuscriptores() {
			# Load slim WP
			define( 'WP_USE_THEMES', false );
			require_once( ABSPATH . 'wp-load.php' );
			//require( './wp-load.php' );
			# http://phpexcel.codeplex.com/
			require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
			$db = wixsms_suscribe_db_init();


			$query = 'SELECT *FROM `' . WIXSMS_SUSCRIBE_DB_TABLE . '`;';
			$error = "Error: the query failed...
			    <pre style='width:700px;word-wrap:break-word;white-space:normal;'>$query</pre>";
			$result = $db->get_results( $query, ARRAY_A ) or wp_die( $error );
            //echo $query;


			$objPHPExcel = new PHPExcel();

			$objPHPExcel->setActiveSheetIndex(0);
			// Initialise the Excel row number
			$rowCount = 0;
			// Sheet cells
			$cell_definition = array(
				'A' => 'suscribe_id',
				'B' => 'first_name',
				'C' => 'email',
				'D' => 'telefono',
				'E' => 'ticket',
        'F' => 'status'
			);
			//Build headers
			foreach( $cell_definition as $column => $value )
				$objPHPExcel->getActiveSheet()->setCellValue( "{$column}1", $value );
			// Build cells
			while( $rowCount < count($result) ){
				$cell = $rowCount + 2;
				foreach( $cell_definition as $column => $value )
					$objPHPExcel->getActiveSheet()->setCellValue($column.$cell, $result[$rowCount][$value]);

			    $rowCount++;
			}

			// Redirect output to a client’s web browser
				ob_clean();
				ob_start();
				$formato = $_GET['format'];
				switch ($formato) {
					case 'csv':
						// Redirect output to a client’s web browser (CSV)
						header("Content-type: text/csv");
						header("Cache-Control: no-store, no-cache");
						header('Content-Disposition: attachment; filename="export.csv"');
						$objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
						$objWriter->setDelimiter(',');
						$objWriter->setEnclosure('"');
						$objWriter->setLineEnding("\r\n");
						//$objWriter->setUseBOM(true);
						$objWriter->setSheetIndex(0);
						$objWriter->save('php://output');
						break;
					case 'xls':
						// Redirect output to a client’s web browser (Excel5)
						header('Content-Type: application/vnd.ms-excel');
						header('Content-Disposition: attachment;filename="export.xls"');
						header('Cache-Control: max-age=0');
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
						$objWriter->save('php://output');
						break;
					case 'xlsx':
						// Redirect output to a client’s web browser (Excel2007)
						header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
						header('Content-Disposition: attachment;filename="export.xlsx"');
						header('Cache-Control: max-age=0');
						$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
						$objWriter->save('php://output');
						break;
				}
			exit;

	}
}


/**
 * Vista del plugin
 */
if (!function_exists('wixsms_registrar_admin_bar')) {
	function wixsms_suscribe_front() {

	}
}

/**===================================
 * OPCIONES
 ===================================*/

/**
 * Opciones de suscripcion
 * @return array retorna las opciones guardadas en WP
 */
function wixsms_suscribe_settings() {
	static $wixsms_suscribe_settings = null;

	if (is_null($wixsms_suscribe_settings)) {
		$wixsms_suscribe_settings = get_option( 'wixsms_suscribe_settings' );
	}

	return $wixsms_suscribe_settings;
}




/**===================================
 * FACEBOOK
 ===================================*/

/**
 * wixsms_suscribe_require_facebook description()
 * Verifica que este configurado el id y secret de facebook
 * Si facebook esta cargado, solo lo retorna,
 * sino, lo carga en una variable statica
 * @return array sdk de facebook
 */
function wixsms_suscribe_require_facebook() {

	$wixsms_suscribe_settings = wixsms_suscribe_settings();
	static $sdk_facebook;

	if ( FALSE === $wixsms_suscribe_settings) {
		die( 'Por favor asigne el ID de su aplicación de Facebook y la Clave secreta' );
		return false;
	}

	if ( ( "" === $wixsms_suscribe_settings['wixsms_text_field_f0'] ) || ( "" === $wixsms_suscribe_settings['wixsms_text_field_f1'] ) ) {
		die( 'Por favor asigne el ID de su aplicación de Facebook y la Clave secreta' );
		return false;
	}


	if (is_null($sdk_facebook)) {
		require_once dirname(__FILE__) . '/vendor/autoload.php';

		$sdk_facebook = new Facebook\Facebook([
			'app_id' => esc_attr( $wixsms_suscribe_settings['wixsms_text_field_f0'] ),
			'app_secret' => esc_attr( $wixsms_suscribe_settings['wixsms_text_field_f1'] ),
			'default_graph_version' => 'v2.4',
		]);
	}



	return $sdk_facebook;
}

/**
 * Genera una ulr para apuntar al login de facebook
 * @return string url de solicitar datos en facebook
 */
function wixsms_suscribe_url_suscripcion() {
	$fb = wixsms_suscribe_require_facebook();
	static $wixsms_suscribe_url_suscripcion = null;

	if (is_null($wixsms_suscribe_url_suscripcion)) {
		if ( FALSE === $fb ) {
			return FALSE;
		}
		$helper = $fb->getRedirectLoginHelper();

		$permissions = ['email'];
		$site_url_callback = site_url('?wixsms_suscribe_fb_callback');

		$wixsms_suscribe_url_suscripcion = $helper->getLoginUrl($site_url_callback, $permissions);
	}

	return $wixsms_suscribe_url_suscripcion;
}

/**
 * Procesa los datos que retorna facebook
 * registra la persona en la base de datos
 * registra la persona en mailchimp
 * @return null
 */
function wixsms_suscribe_fb_callback() {
	if (isset($_GET['wixsms_suscribe_fb_callback'])) {
		$fb = wixsms_suscribe_require_facebook();
		require_once dirname(__FILE__) . '/callback-fb.php';
		exit();
	}
}



function appIdFacebook(){
	$wixsms_suscribe_settings = wixsms_suscribe_settings();
	$appID = esc_attr( $wixsms_suscribe_settings['wixsms_text_field_f0'] );
	echo $appID;
}



/**===================================
 * BASE DE DATOS
 ===================================*/

function wixsms_suscribe_db_init() {
	static $db = null;
	if (is_null($db)) {
		global $wpdb;
		$db = $wpdb;
	}

	if (!defined('WIXSMS_SUSCRIBE_DB_TABLE')) {
		define('WIXSMS_SUSCRIBE_DB_TABLE', $db->prefix . 'wixsms_suscrito');
	}
	if (!defined('WIXSMS_SMS_DB_TABLE')) {
		define('WIXSMS_SMS_DB_TABLE', $db->prefix . 'wixsms_sms');
	}
	if (!defined('WIXSMS_TAREA_DB_TABLE')) {
		define('WIXSMS_TAREA_DB_TABLE', $db->prefix . 'wixsms_tarea');
	}
	if (!defined('WIXSMS_SERVIDORES_DB_TABLE')) {
		define('WIXSMS_SERVIDORES_DB_TABLE', $db->prefix . 'wixsms_servidores_sms');
	}

	wixsms_suscribe_db_create_if_not_exist($db);

	return $db;
}

function wixsms_suscribe_db_create_if_not_exist($db = null) {

	if($db->get_var("SHOW TABLES LIKE '".WIXSMS_SUSCRIBE_DB_TABLE."'") != WIXSMS_SUSCRIBE_DB_TABLE) {
		$charset_collate = $db->get_charset_collate();
		$sql_create_table = "
			CREATE TABLE IF NOT EXISTS `".WIXSMS_SUSCRIBE_DB_TABLE."` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`email` varchar(100) NOT NULL,
				`first_name` varchar(50) DEFAULT NULL,
				`facebook_id` varchar(50) DEFAULT NULL,
				`facebook_token` int(20) DEFAULT NULL,
				`dispositivo` varchar(10) DEFAULT NULL,
				`totalsms` int(11) DEFAULT NULL,
				`fecha_reg` varchar(40) DEFAULT NULL,
				`like` int(11) DEFAULT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB {$charset_collate};
		";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql_create_table );
	}

	if($db->get_var("SHOW TABLES LIKE '".WIXSMS_SMS_DB_TABLE."'") != WIXSMS_SMS_DB_TABLE) {
		$charset_collate = $db->get_charset_collate();
		$sql_create_table = "
			CREATE TABLE IF NOT EXISTS `".WIXSMS_SMS_DB_TABLE."` (
				`idsms` int(11) NOT NULL AUTO_INCREMENT,
				`code` char(3) NOT NULL,
				`telefono` varchar(15) DEFAULT NULL,
				`mensaje` varchar(160) DEFAULT NULL,
				`facebook_token` varchar(50) DEFAULT NULL,
				`status` int(11) DEFAULT NULL,
				`fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`idsms`)
			) ENGINE=InnoDB {$charset_collate};
		";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql_create_table );
	}

	if($db->get_var("SHOW TABLES LIKE '".WIXSMS_TAREA_DB_TABLE."'") != WIXSMS_TAREA_DB_TABLE) {
		$charset_collate = $db->get_charset_collate();
		$sql_create_table = "
			CREATE TABLE IF NOT EXISTS `".WIXSMS_TAREA_DB_TABLE."` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`status` varchar(20) NOT NULL,
				`id_usuario` int(11) DEFAULT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB {$charset_collate};
		";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql_create_table );
	}

	if($db->get_var("SHOW TABLES LIKE '".WIXSMS_SERVIDORES_DB_TABLE."'") != WIXSMS_SERVIDORES_DB_TABLE) {
		$charset_collate = $db->get_charset_collate();
		$sql_create_table = "
			CREATE TABLE IF NOT EXISTS `".WIXSMS_SERVIDORES_DB_TABLE."` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`usuario` varchar(50) NOT NULL,
				`email` varchar(100) DEFAULT NULL,
				`totalenvios` int(11) DEFAULT NULL,
				`status` int(11) DEFAULT NULL,
				`code` varchar(5) DEFAULT NULL,
				`token` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB {$charset_collate};
		";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql_create_table );
	}

}

function get_suscriptores() {

    $db = wixsms_suscribe_db_init();
    if (
            (isset($_GET['paged'])) &&
            ($_GET['paged'] != '')) {
        $offset = ($_GET['paged']-1) * 10;
    }
    else {
        $offset = 0;
    }
    $sql_select_suscriptores = '
			SELECT *FROM `' . WIXSMS_SUSCRIBE_DB_TABLE . '` LIMIT ' . 10 . ' OFFSET ' . $offset . ';';
    //echo $sql_select_suscriptores;
    $results = $db->get_results($sql_select_suscriptores, OBJECT);
    return $results;
}

function get_todos_suscriptores() {

    $db = wixsms_suscribe_db_init();
    $sql_select_suscriptores = '
			SELECT *FROM `' . WIXSMS_SUSCRIBE_DB_TABLE . '`;';
    //echo $sql_select_suscriptores;
    $results = $db->get_results($sql_select_suscriptores, OBJECT);
    return $results;
}




function wixsms_suscribe_db_insert_suscriptor($data = null, $identificador) {

	$db = wixsms_suscribe_db_init();
	$detect = load_mobile_detects();
	$dispositivo = '';
		if ( $detect->isMobile() ) {
			$dispositivo = 'Movil';
		}
		elseif ($detect->isTablet() ) {
			$dispositivo = 'Tablet';
		}
		else {
			$dispositivo = 'Desktop';
		}

		$id = $data['facebook_token']=='' ? '-':$data['facebook_token'];

	$userdata = [
		'email' => $data['email'],
		'first_name' => $data['first_name']." ".$data['last_name'],
		'facebook_id' => $data['facebook_id'],
		'facebook_token' => $id,
		'dispositivo' => $dispositivo,
		'fecha_reg' => date('Y-m-d H:g:i')
	];

	$results = $db->insert(
		WIXSMS_SUSCRIBE_DB_TABLE,
		$userdata,
		array(
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
		)
	);
	return $results;
}


/*** pasos del sorteo ***/
/*add_action( 'init', 'cg_sorteo');
function cg_sorteo(){

		if(isset($_GET['paso']) && ($_GET['paso']!='')){
				$paso_d = explode(':', $_GET['paso']);
				$paso = $paso_d[0];
 				if($paso==1 && isset($_POST['facebook_id'])){
						// PASOS PARA CONECTAR A LA BASE DE DATOS

						if(is_null(validarIdentificacion($_POST['iden']))){

							$db = conexion();

							if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
								$email = $_POST['email'];
							}
							else{
								$email = '';
							}

							$datos = array(
								'first_name' => $_POST['nombre'],
								'telefono' => $_POST['code']."".$_POST['numero'],
								'email'			=> $email,
								'status' 	=> 'Pendiente',
								'facebook_token' => $_POST['iden']
							);
							/// actualizamos y redireccionamos al siguiente paso
							$actualizar = $db->update( WIXSMS_SUSCRIBE_DB_TABLE, $datos, array( 'facebook_id' => $_POST['facebook_id'] ), array( '%s' ) );
							wp_redirect(home_url("?paso=2&email=".$email.""));
							exit();
						}
						else{
							wp_redirect(home_url());
							exit();
						}
				}

				if($paso==4 && isset($paso_d[1]) && $paso_d[2]==0){

						if(is_null(validarEmail($paso_d[1]))){
							wp_redirect(home_url());
							exit();
						}
							// PASOS PARA CONECTAR A LA BASE DE DATOS
							$db = conexion();
							// generamos el ticket
							$ticket = generarCodigo(6);

							$datos = array(
								'ticket' => $ticket,
								'status'	=> 'Suscrito',
								'fecha_reg' => date('Y-m-d H:i:g')
							);
							// actualizamos los datos del cliente y redirecionamos
							$actualizar = $db->update( WIXSMS_SUSCRIBE_DB_TABLE, $datos, array( 'email' => $paso_d[1] ), array( '%s' ) );

							enviar_email($paso_d[1], $ticket);

							wp_redirect(home_url("?paso=4&ticket=".$ticket.""));
							exit();

						}

						if($paso==4 && isset($_GET['ticket'])){
							if(is_null( validarTicket($_GET['ticket']))){
								wp_redirect(home_url());
								exit();
							}
						}

				}

}



*/








function wixsms_suscribe_db_select_suscriptor($email = null) {
	$db = wixsms_suscribe_db_init();
	return $db->get_var('SELECT * FROM '.WIXSMS_SUSCRIBE_DB_TABLE.' WHERE email = "' . $email . '";');
}

function validarId($id) {
	$db = wixsms_suscribe_db_init();
	$registro = $db->get_results('SELECT ticket FROM '.WIXSMS_SUSCRIBE_DB_TABLE.' WHERE facebook_token = "' . $id . '";');
	return $registro[0];
}

function vaciarSuscriptores() {
	$db = wixsms_suscribe_db_init();
	return $db->get_var('TRUNCATE '.WIXSMS_SUSCRIBE_DB_TABLE.';');
}

function enviarEmailPendientes() {
	$db = wixsms_suscribe_db_init();
	$lista =  $db->get_results('SELECT email FROM '.WIXSMS_SUSCRIBE_DB_TABLE.' WHERE ticket="-" AND status="Pendiente"', OBJECT);
	if($lista){
		foreach ($lista as $suscriptor){
			enviar_email_pendiente($suscriptor->email);
		}
	}

}




function wixsms_suscribe_guardar_suscriptor($data = array(), $identificador) {
	$email = $data['email']=='' ? '-':$data['email'];
	$data_db = [
		'email' => $email,
		'first_name' => $data['first_name']." ".$data['last_name'],
		'facebook_id' => $identificador,
		'facebook_token' => $data['id']
	];

	if (is_null(wixsms_suscribe_db_select_suscriptor($email))) {
			wixsms_suscribe_db_insert_suscriptor($data_db, $identificador);
	}

}

function load_mobile_detects() {

	static $detects = null;
	$detects = new Mobile_Detect();
	return $detects;
}




function validarEmail($email = null) {
	$db = conexion();
	return $db->get_var('SELECT email FROM '.WIXSMS_SUSCRIBE_DB_TABLE.' WHERE email = "' . $email . '";');
}

function validarIdentificacion($iden = null) {
	$db = conexion();
	return $db->get_var('SELECT email FROM '.WIXSMS_SUSCRIBE_DB_TABLE.' WHERE facebook_token = "' . $iden . '";');
}



function actualizarLike($email,$like){
	$db = conexion();
	$datos = array(
		'like' => $like
	);
	// actualizamos los datos del cliente y redirecionamos
	$actualizar = $db->update( WIXSMS_SUSCRIBE_DB_TABLE, $datos, array( 'email' => $email ), array( '%s' ) );
}

function conexion(){
	 static $db = null;
	 if (is_null($db)) {
		 global $wpdb;
		 $db = $wpdb;
	 }

	 if (!defined('WIXSMS_SUSCRIBE_DB_TABLE')) {
		 define('WIXSMS_SUSCRIBE_DB_TABLE', $db->prefix . 'wixsms_suscribe');
	 }

	 return $db;
 }


 /*
   ==================================================================
   Accion para enviar email de consulta
   ==================================================================
  */

 function send_smtp_email( $phpmailer ) {

	 //accedemos a las opciones
	 $opciones = wixsms_suscribe_settings();

 // La dirección del HOST del servidor de correo SMTP p.e. smtp.midominio.com
 	$phpmailer->Host = $opciones['wixsms_text_field_e0'];

 // Puerto SMTP - Suele ser el 25, 465 o 587
 	$phpmailer->Port = $opciones['wixsms_text_field_e1'];

 // Usuario de la cuenta de correo
 	# info@envioscaracol.com
 	$phpmailer->Username = $opciones['wixsms_text_field_e2'];

 // Contraseña para la autenticación SMTP
 	# Demo4231#@64
 	$phpmailer->Password = $opciones['wixsms_text_field_e3'];

 // Uso autenticación por SMTP (true|false)
 	$phpmailer->SMTPAuth = true;

 // El tipo de encriptación que usamos al conectar - ssl (deprecated) o tls
 #$phpmailer->SMTPSecure = "tls";
 	# "info@onefocusdigital.com"
 	$phpmailer->From = $opciones['wixsms_text_field_e4'];
 	#"OnefocusDigital -  Agencia de Publicidad"
 	$phpmailer->FromName = $opciones['wixsms_text_field_e5'];

 // Define que estamos enviando por SMTP
 	$phpmailer->isSMTP();
 }


 /*
   ==================================================================
   funcion que envia el email de consulta
   ==================================================================
  */

 function enviar_email($email,$ticket) {

 			$headers = array( 'Content-Type: text/html; charset=UTF-8' );
 			$correo = 'carlosguilarte3@gmail.com';

 			add_action( 'phpmailer_init', 'send_smtp_email' );

 			$mensaje = '<b>¡Felicidades!</b>
 				<p>Se ha registrado exitosamente, este es tu número de ticket en el sorteo:</p>
 				<br>
 				<span style="display:block;font-size:24px;font-weight:bold;">'.$ticket.'</span>
 				<br>
				<p>La selección del número ganador se realizará en una de nuestras agencias Portal al Caribe, específicamente en la agencia sita en 4311 Palm Ave. Hialeah, FL 33012 el día 14 de Febrero de 2017.</p>
				<p>Ese mismo día será anunciado por los medios, y el ganador será notificado por e-mail y por vía telefónica.</p>
				<p>Puede obtener cualquier información adicional llamando al teléfono (786) 220 8559, o directamente en nuestras oficinas:</p>
					<ul>
						<li>Flagler: 6726 West Flagler St. Miami. FL. 33144</li>
						<li>Palm Ave: 4311 Palm Ave. Hialeah, FL 33012</li>
						<li>Doral: 7211 NW 46 th St Miami. FL 33166</li>
					</ul>';



 			$result = wp_mail( $email, 'Ticket de Sorteo', $mensaje, $headers, '' );

 			if ( !$result ) {
 				$ts_mail_errors = -1;
 			} else {
 				$ts_mail_errors = 1;
 				//wp_mail( $correo, 'Ticket de Sorteo', $mensaje, $headers, '' );
 			}

 }


 function enviar_email_pendiente($email) {

 			$headers = array( 'Content-Type: text/html; charset=UTF-8' );

 			add_action( 'phpmailer_init', 'send_smtp_email' );
			$URL = home_url('?paso=3&email='.$email.'');
 			$mensaje = '<b>¡Hola!</b>
 				<p>Aún no estas registrado. Para obtener su número de ticket en el sorteo asegurate de completar este último paso de compartir en Facebook:</p>
 				<br>
 				<span style="display:block;font-size:24px;font-weight:bold;"><a href="'.$URL.'">Completar paso</a></span>';

 			$result = wp_mail( $email, 'Completar ultimo paso del sorteo', $mensaje, $headers, '' );

 			if (!$result ) {
 				$ts_mail_errors = -1;
 			} else {
 				$ts_mail_errors = 1;
				// PASOS PARA CONECTAR A LA BASE DE DATOS
				$db = conexion();

				$datos = array(
					'ticket' => '--');
				// actualizamos los datos del cliente y redirecionamos
				$actualizar = $db->update( WIXSMS_SUSCRIBE_DB_TABLE, $datos, array( 'email' => $email ), array( '%s' ) );
 				//wp_mail( $correo, 'Ticket de Sorteo', $mensaje, $headers, '' );
 			}

 }
