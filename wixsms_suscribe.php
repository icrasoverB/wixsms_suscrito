<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.facebook.com/icrasover.21
 * @since             1.0.0
 * @package           WixSms
 *
 * @wordpress-plugin
 * Plugin Name:       WixSms pligin
 * Plugin URI:        http://www.itsoft.com.ve
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Carlos Guilarte
 * Author URI:        https://www.facebook.com/icrasover.21
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       WixSms
 * Domain Path:       /languages
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/setting.php';
require_once dirname(__FILE__) . '/widget.php';


add_filter( 'admin_menu', 'wixsms_suscribe_menu' );
add_action( 'admin_bar_menu', 'wixsms_registrar_admin_bar', 1000 );
add_action( 'init', 'wixsms_suscribe_fb_callback');



//=======================================================
// MENU
//=======================================================
