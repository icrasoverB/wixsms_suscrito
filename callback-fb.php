<?php

if(!session_id()) {
session_start();
}

if (isset($_SESSION['facebook_access_token'])) {
	$accessToken = $_SESSION['facebook_access_token'];
}
else {
	$helper = $fb->getRedirectLoginHelper();
	if (isset($_GET['state'])) {
    $helper->getPersistentDataHandler()->set('state', $_GET['state']);
	}
	try {
		$accessToken = $helper->getAccessToken();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}

	if (isset($accessToken)) {
		// Logged in!
		$_SESSION['facebook_access_token'] = (string) $accessToken;

		// Now you can redirect to another page and use the
		// access token from $_SESSION['facebook_access_token']
	}
}

if (isset($accessToken)) {

	try {
		// Returns a `Facebook\FacebookResponse` object
		$response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken)->getDecodedBody();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Graph returned an error: ' . $e->getMessage();
		unset($_SESSION['facebook_access_token']);
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDKs returned an error: ' . $e->getMessage();
		exit;
	}

	/**
	 * Guardar Persona en la base de datos
	 */


}
/**
 * Eliminar sesion por seguridad
 */
	//unset($_SESSION['facebook_access_token']);
	if($response['id']!=''){
		$token = $response['id'];
		$existe = validarId($response['id']);
		if(is_null($existe)){
			$identificador = time();
			wixsms_suscribe_guardar_suscriptor($response,$identificador);
			wp_redirect( site_url('?paso=1&fb_id='.$token.'') );
			unset($_SESSION['facebook_access_token']);
			exit();
		}
		else{
			wp_redirect(home_url('?paso=1&fb_id='.$token.''));
			unset($_SESSION['facebook_access_token']);
		}
	}
	else{
		wp_redirect(site_url());
		exit();
	}
