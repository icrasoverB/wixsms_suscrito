<?php


add_action( 'admin_init', 'wixsms_suscribe_settings_init' );


function wixsms_suscribe_settings_init() {
	register_setting( 'wixsms_suscribe_settings_pagina', 'wixsms_suscribe_settings' );

	wixsms_settings_mailchimp();

	wixsms_settings_facebook();

	wixsms_settings_email();

}


/**
 * Mailchimp
 */

function wixsms_settings_mailchimp() {

	add_settings_section(
		'wixsms_pluginPage_section',
		__( '1.- Configuración de MailChimp', 'wixsms_suscribe' ),
		'wixsms_settings_section_0_callback',
		'wixsms_suscribe_settings_pagina'
	);

	add_settings_field(
		'wixsms_text_field_0',
		__( '1.1 API de Mailchimp', 'wixsms_suscribe' ),
		'wixsms_text_field_0_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section'
	);

	add_settings_field(
		'wixsms_text_field_1',
		__( '1.2 ID de la lista de Mailchimp', 'wixsms_suscribe' ),
		'wixsms_text_field_1_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section'
	);
}

function wixsms_settings_section_0_callback() {}


# wixsms_text_field_0 >> API MailChimp
function wixsms_text_field_0_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	echo <<<IMPUT
	<input id="wixsms_text_field_0" type="text" name="wixsms_suscribe_settings[wixsms_text_field_0]" value="{$options['wixsms_text_field_0']}"><br>
	Encontrar o crear un <a target="_blank" href="http://kb.mailchimp.com/es/integrations/api-integrations/about-api-keys">API KEY</a> de MailChimp.
IMPUT;
}

# wixsms_text_field_1 >> Listas MailChimp
function wixsms_text_field_1_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	echo <<<IMPUT
	<input id="wixsms_text_field_1" type="text" name="wixsms_suscribe_settings[wixsms_text_field_1]" value="{$options['wixsms_text_field_1']}"><br>
	Encontrar el <a target="_blank" href="http://kb.mailchimp.com/es/lists/managing-subscribers/find-your-list-id">ID de la Lista</a> de MailChimp.
IMPUT;
}








/**
 * Facebook
 */

function wixsms_settings_facebook() {

	add_settings_section(
		'wixsms_pluginPage_section_fb',
		__( '2.- Configuración de Facebook', 'wixsms_suscribe' ),
		'wixsms_settings_section_f0_callback',
		'wixsms_suscribe_settings_pagina'
	);

	add_settings_field(
		'wixsms_text_field_f0',
		__( '2.1 Identificador de la aplicación (ID)', 'wixsms_suscribe' ),
		'wixsms_text_field_f0_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_fb'
	);

	add_settings_field(
		'wixsms_text_field_f1',
		__( '2.2 Clave secreta de la aplicación', 'wixsms_suscribe' ),
		'wixsms_text_field_f1_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_fb'
	);
}

function wixsms_settings_section_f0_callback() { return true; }


# wixsms_text_field_f0 >> App ID
function wixsms_text_field_f0_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	echo <<<IMPUT
	<input id="wixsms_text_field_f0" type="text" name="wixsms_suscribe_settings[wixsms_text_field_f0]" value="{$options['wixsms_text_field_f0']}"><br>
	Ubicar el <a target="_blank" href="https://developers.facebook.com/apps">App ID</a>.
IMPUT;
}

# wixsms_text_field_f1 >> App Secret
function wixsms_text_field_f1_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	echo <<<IMPUT
	<input id="wixsms_text_field_f1" type="text" name="wixsms_suscribe_settings[wixsms_text_field_f1]" value="{$options['wixsms_text_field_f1']}"><br>
	Guia para encontrar la <a target="_blank" href="https://developers.facebook.com/docs/apps/register#app-id">Clave Secreta</a>.
IMPUT;
}



/**
 * Configuracion de correo
 */

function wixsms_settings_email() {

	add_settings_section(
		'wixsms_pluginPage_section_email',
		__( '3.- Configuración de Email', 'wixsms_suscribe' ),
		'wixsms_settings_section_e0_callback',
		'wixsms_suscribe_settings_pagina'
	);

	add_settings_field(
		'wixsms_text_field_e0',
		__( '3.1 SMTP Host', 'wixsms_suscribe' ),
		'wixsms_text_field_e0_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_email'
	);

	add_settings_field(
		'wixsms_text_field_e1',
		__( '3.2 SMTP Port', 'wixsms_suscribe' ),
		'wixsms_text_field_e1_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_email'
	);

	add_settings_field(
		'wixsms_text_field_e2',
		__( '3.3 SMTP Username', 'wixsms_suscribe' ),
		'wixsms_text_field_e2_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_email'
	);

	add_settings_field(
		'wixsms_text_field_e3',
		__( '3.4 SMTP Password', 'wixsms_suscribe' ),
		'wixsms_text_field_e3_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_email'
	);

	add_settings_field(
		'wixsms_text_field_e4_render',
		__( '3.5 SMTP From', 'wixsms_suscribe' ),
		'wixsms_text_field_e4_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_email'
	);

	add_settings_field(
		'wixsms_text_field_e5_render',
		__( '3.6 SMTP FromName', 'wixsms_suscribe' ),
		'wixsms_text_field_e5_render',
		'wixsms_suscribe_settings_pagina',
		'wixsms_pluginPage_section_email'
	);

}

function wixsms_settings_section_e0_callback() { return true; }


function wixsms_text_field_e0_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	if (!isset($options['wixsms_text_field_e0'])) {
		$options['wixsms_text_field_e0'] = '';
	}
	echo <<<IMPUT
	<input id="wixsms_text_field_e0" type="text" name="wixsms_suscribe_settings[wixsms_text_field_e0]" value="{$options['wixsms_text_field_e0']}">
IMPUT;
}

function wixsms_text_field_e1_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	if (!isset($options['wixsms_text_field_e1'])) {
		$options['wixsms_text_field_e1'] = '';
	}
	echo <<<IMPUT
	<input id="wixsms_text_field_e1" type="text" name="wixsms_suscribe_settings[wixsms_text_field_e1]" value="{$options['wixsms_text_field_e1']}">
IMPUT;
}

function wixsms_text_field_e2_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	if (!isset($options['wixsms_text_field_e2'])) {
		$options['wixsms_text_field_e2'] = '';
	}
	echo <<<IMPUT
	<input id="wixsms_text_field_e2" type="text" name="wixsms_suscribe_settings[wixsms_text_field_e2]" value="{$options['wixsms_text_field_e2']}">
IMPUT;
}

function wixsms_text_field_e3_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	if (!isset($options['wixsms_text_field_e3'])) {
		$options['wixsms_text_field_e3'] = '';
	}
	echo <<<IMPUT
	<input id="wixsms_text_field_e3" type="text" name="wixsms_suscribe_settings[wixsms_text_field_e3]" value="{$options['wixsms_text_field_e3']}">
IMPUT;
}

function wixsms_text_field_e4_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	if (!isset($options['wixsms_text_field_e4'])) {
		$options['wixsms_text_field_e4'] = '';
	}
	echo <<<IMPUT
	<input id="wixsms_text_field_e4" type="text" name="wixsms_suscribe_settings[wixsms_text_field_e4]" value="{$options['wixsms_text_field_e4']}">
IMPUT;
}

function wixsms_text_field_e5_render(  ) {
	$options = get_option( 'wixsms_suscribe_settings' );
	if (!isset($options['wixsms_text_field_e5'])) {
		$options['wixsms_text_field_e5'] = '';
	}
	echo <<<IMPUT
	<input id="wixsms_text_field_e5" type="text" name="wixsms_suscribe_settings[wixsms_text_field_e5]" value="{$options['wixsms_text_field_e5']}">
IMPUT;
}
