<?php

class OFD_Suscribe_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {

		#add_action('admin_enqueue_scripts', array(&$this, 'register_admin_scripts'));

		$widget_ops = array(
			'classname' => 'ofd_suscribe_widget',
			'description' => 'Suscribirse, nunca fue tan fácil',
		);


		parent::__construct( 'ofd_suscribe_widget', '<>OFD Suscribe', $widget_ops );

	}


	function register_admin_scripts($hook) {
		if ($hook != 'widgets.php') {
			return;
		}
		wp_register_script('ofd-subscribe-admin', OFD_SUSCRIBE_PLUGIN_DIR . 'public/js/ofd_suscribe_admin.js', array('jquery'));  
		wp_enqueue_script('ofd-subscribe-admin');
	}


	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		require_once dirname(__FILE__) . '/widget-widget.php';

		echo $args['after_widget'];
	}


	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Apúntame ahí!', 'ofd_suscribe' );
		$descripcion = ! empty( $instance['descripcion'] ) ? $instance['descripcion'] : __( 'Verás que cantidad de chismes te eviamos!', 'ofd_suscribe' );
		$estructura = ! empty( $instance['estructura'] ) ? $instance['estructura'] : 'cuadrado';
		$texto_footer = ! empty( $instance['texto_footer'] ) ? $instance['texto_footer'] : __( '* Palabra de santo que no enviaremos correo spam.', 'ofd_suscribe' );
		$segundos_ventana = ! empty( $instance['segundos_ventana'] ) ? $instance['segundos_ventana'] : 0;
		$mostrar_flotante = isset( $instance['mostrar_flotante'] ) ? $instance['mostrar_flotante'] : '';
		?>

		<div class="ofd_subscribe_options_form">

			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'descripcion' ); ?>"><?php _e( 'Descripción:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'descripcion' ); ?>" name="<?php echo $this->get_field_name( 'descripcion' ); ?>" type="text" value="<?php echo esc_attr( $descripcion ); ?>">
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'estructura' ); ?>"><?php _e( 'Diseño:' ); ?></label>
				<select class="widefat" id="<?php echo $this->get_field_id( 'estructura' ); ?>" name="<?php echo $this->get_field_name( 'estructura' ); ?>" type="text">
				<option value="cuadrado" <?php echo (esc_attr( $estructura ) == 'cuadrado') ? 'selected' : '' ?>>Por defecto</option>
				<option value="rectangular" <?php echo (esc_attr( $estructura ) == 'rectangular') ? 'selected' : '' ?>>Rectangular</option>
				</select>
			</p>

			<p>
				<input class="checkbox" type="checkbox" value="on" id="<?php echo $this->get_field_id( 'mostrar_flotante' ); ?>" name="<?php echo $this->get_field_name( 'mostrar_flotante' ); ?>" <?php echo checked( 'on', $mostrar_flotante, false ); ?> />
				<label for="<?php echo $this->get_field_id( 'mostrar_flotante' ); ?>"><?php _e( 'Mostrar en una ventana flotante' ); ?></label>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'segundos_ventana' ); ?>"><?php _e( 'Despues de cuantos segundos desea que aparezca la ventana?:' ); ?></label>
				<input class="tiny-text" id="<?php echo $this->get_field_id( 'segundos_ventana' ); ?>" name="<?php echo $this->get_field_name( 'segundos_ventana' ); ?>" type="number" step="1" min="1" value="<?php echo $segundos_ventana; ?>" size="3" />
			</p>




			<h4 class="wp_subscribe_labels_header"><a class="wp-subscribe-toggle" href="#" rel="wp_subscribe_labels">
				<?php _e('Textos y mensajes', 'ofd_suscribe'); ?></a>
			</h4>
			<div class="wp_subscribe_labels" style="display: none;">
			<?php 
				$instance['facebook_button_text'] = 		(strlen($instance['facebook_button_text']) > 0) ? $instance['facebook_button_text'] : __( 'Suscríbete con Facebook', 'ofd_suscribe' );
				$instance['email_placeholder'] = 			(strlen($instance['email_placeholder']) > 0) ? $instance['email_placeholder'] : __( 'Déjanos tu correo', 'ofd_suscribe' );
				$instance['email_button_text'] = 			(strlen($instance['email_button_text']) > 0) ? $instance['email_button_text'] : __( 'Suscríbete con Correo', 'ofd_suscribe' );
				$instance['success_message'] = 				(strlen($instance['success_message']) > 0) ? $instance['success_message'] : __( 'Suscrito con exito', 'ofd_suscribe' );
				$instance['error_message'] = 				(strlen($instance['error_message']) > 0) ? $instance['error_message'] : __( 'Su email es invalido', 'ofd_suscribe' );
				$instance['already_subscribed_message'] = 	(strlen($instance['already_subscribed_message']) > 0) ? $instance['already_subscribed_message'] : __( 'Ya se encuentra suscrito', 'ofd_suscribe' );
				$instance['texto_footer'] = 					(strlen($instance['texto_footer']) > 0) ? $instance['texto_footer'] : __( '* Palabra de santo que no enviaremos correo spam.', 'ofd_suscribe' );


				$this->output_text_field('facebook_button_text', __('(Facebook) Texto del botón', 'ofd_subscribe'), $instance['facebook_button_text']);
				$this->output_text_field('email_placeholder', __('(Email) Input Placeholder', 'ofd_subscribe'), $instance['email_placeholder']);
				$this->output_text_field('email_button_text', __('(Email) Texto del botón', 'ofd_subscribe'), $instance['email_button_text']);
				$this->output_text_field('success_message', __('Suscrito con éxito', 'ofd_subscribe'), $instance['success_message']);
				$this->output_text_field('error_message', __('Email inválido', 'ofd_subscribe'), $instance['error_message']);
				$this->output_text_field('already_subscribed_message', __('Error: ya está suscrito', 'ofd_subscribe'), $instance['already_subscribed_message']);
				$this->output_text_field('texto_footer', __('Footer Text', 'ofd_subscribe'), $instance['texto_footer']);
			?>
			</div><!-- .wp_subscribe_labels -->




		</div>


		<?php
	}



	public function output_text_field($setting_name, $setting_label, $setting_value) {
		?>

		<p class="wp-subscribe-<?php echo $setting_name; ?>-field">
			<label for="<?php echo $this->get_field_id($setting_name) ?>">
				<?php echo $setting_label ?>
			</label>

			<input class="widefat" 
				   id="<?php echo $this->get_field_id($setting_name) ?>" 
				   name="<?php echo $this->get_field_name($setting_name) ?>" 
				   type="text" 
				   value="<?php echo esc_attr($setting_value) ?>" />
		</p>

		<?php
	}

	public function output_textarea_field($setting_name, $setting_label, $setting_value) {
		?>

		<p class="wp-subscribe-<?php echo $setting_name; ?>-field">
			<label for="<?php echo $this->get_field_id($setting_name) ?>">
				<?php echo $setting_label ?>
			</label>
			
			<textarea class="widefat" id="<?php echo $this->get_field_id($setting_name) ?>" name="<?php echo $this->get_field_name($setting_name) ?>"><?php echo esc_attr($setting_value); ?></textarea>
		</p>

		<?php
	}


	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		foreach( $new_instance as $key => $value )
		{
			$updated_instance[$key] = sanitize_text_field($value);
		}

		return $updated_instance;
	}
}


add_action( 'widgets_init', function(){
	register_widget( 'OFD_Suscribe_Widget' );
});













/**
 * Registrar en el plugin
 */

if ( !function_exists('ofd_get') ) {
	function ofd_get( $key = null ) {
		if (isset($_GET[$key]) && !is_null($key))
			return esc_attr( $_GET[$key] );
		return false;
	}
}
