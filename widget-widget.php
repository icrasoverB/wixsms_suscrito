
<script type="text/javascript">
    $(document).on('ready', function() {
        var ofd_subscribe = $('#ofd-suscribe');

        $(document).on('click', '#ofd-suscribe .ofd_close', function(event) {
            event.preventDefault();
            ofd_suscribe_fixed_to_relative()
        });


        if ( (localStorage.ofd_suscribe_fixed === 1) || (getCookie('ofd_suscribe_fixed') === "1") ){
            ofd_suscribe_relative_to_fixed();
        }

    });

    function ofd_suscribe_relative_to_fixed() {
        var ofd_subscribe = $('#ofd-suscribe');
        ofd_subscribe.addClass('ofd_subscribe-fixed');
        $('.ofd_suscribe_modal-backdrop').removeClass('ofd_hidden');
        $('body').addClass('modal-open');
    }

    function ofd_suscribe_fixed_to_relative() {
        var ofd_subscribe = $('#ofd-suscribe');
        ofd_subscribe.removeClass('ofd_subscribe-fixed');
        $('.ofd_suscribe_modal-backdrop').addClass('ofd_hidden');
        $('body').removeClass('modal-open');
    }

    function ofd_suscribe_visitas_cookie(elem_fixed) {
        localStorage.ofd_suscribe_fixed = (localStorage.ofd_suscribe_fixed || elem_fixed);
        setCookie('ofd_suscribe_fixed', elem_fixed, 1);
        localStorage.ofd_suscribe_fixed++; // incrementamos cuenta de la cookie
    }


    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }


    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length,c.length);
            }
        }
        return "";
    }

    if ( (getCookie('ofd_suscribe_fixed') === 'fixed') || (getCookie('ofd_suscribe_fixed') === 'relative') ) {}
    else { ofd_suscribe_visitas_cookie('fixed'); }
</script>











<?php
/*
$instance['title']
$instance['descripcion']
$instance['estructura']
$instance['texto_footer']
$instance['success_message']
$instance['error_message']
$instance['already_subscribed_message']
$instance['facebook_button_text']


$instance['mostrar_flotante']
$instance['segundos_ventana']
$instance['email_placeholder']
$instance['email_button_text']
 */


    $ofd_suscribe_descripcion = "";
    switch (ofd_get('ofd_suscribe_status')) {
        case 'email_invalido':
            $ofd_suscribe_descripcion = $instance['error_message'];
            break;

        case 'suscrito':
            $ofd_suscribe_descripcion = $instance['success_message'];
            break;

        case 'ya_suscrito':
            $ofd_suscribe_descripcion = $instance['already_subscribed_message'];
            break;

        default:
            $ofd_suscribe_descripcion = $instance['descripcion'];
            break;
    }


?>

<?php ob_start(); ?>
<div id="ofd-suscribe">

    <button type="button" class="ofd_close" data-dismiss="ofd_modal" aria-label="Close" onclick="ofd_suscribe_visitas_cookie('relative');">
        <span aria-hidden="true">&times;</span>
    </button>

    <h4 class="title Kelson-Regular pt-30"><?php echo $instance['title'] ?></h4>
    <span class="subtitulo Kelson-Regular"><?php echo $ofd_suscribe_descripcion ?></span>

    <div class="ofd_accion pt-30">

        <button id="ofd_suscribe-btn_email" type="submit" class="ofd_d-block ofd_btn Kelson-Regular hidden-xs-up">
            <i class="fa fa-envelope-o" aria-hidden="true"></i> Suscríbete con Correo
        </button>
        <div class="ofd_suscribe-form_email">
            <a href="<?php echo ofd_suscribe_url_suscripcion() ?>" id="ofd_suscribe-btn_facebook" class="ofd_d-block ofd_btn ofd-btn_facebook Kelson-Regular">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                <?php echo $instance['facebook_button_text'] ?>
            </a>
        </div>

        <form action="?" method="POST" class="ofd_suscribe-form_email hidden-xs-up">
            <input class="ofd_form-control Roboto" placeholder="Dale, ponte aquí.. Dame tu email ahora!" type="email">
            <button type="submit" class="ofd_btn ofd_btn-send Kelson-Regular">Métele<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
            </button>
        </form>
    </div>
    <p class="footer-text pt-145 Kelson-Regular"><?php echo $instance['texto_footer'] ?></p>
</div>
<div class="ofd_suscribe_modal-backdrop ofd_hidden"></div>

<script type="text/javascript">
    $(document).on('ready', function() {
        <?php if (strtolower(ofd_get('ofd_suscribe_status')) === 'suscrito'): ?>
            ofd_suscribe_relative_to_fixed();
        <?php endif ?>

        <?php if (isset($instance['mostrar_flotante']) && ($instance['mostrar_flotante'] === 'on')): ?>
        if (getCookie('ofd_suscribe_fixed') === 'fixed') {
                setTimeout( ofd_suscribe_relative_to_fixed , <?php echo $instance['segundos_ventana'] * 1000  ?> );
        }
        <?php endif ?>
    });
</script>

<?php $widget_cuadrado = ob_get_clean(); ?>



<?php ob_start(); ?>
<div class="tdc-suscripcion">
    <h2 class="Galindo"><?php echo $instance['title'] ?></h2>

    <span class="Galindo"><?php echo $ofd_suscribe_descripcion ?></span>

    <div id="tdc_suscribe-btn_facebook" class="ofd_btn_conteiner ofd_d-block">
        <a href="<?php echo ofd_suscribe_url_suscripcion() ?>" class="ofd_d-block tdc_btn ofd-btn_facebook Roboto">
            <?php echo $instance['facebook_button_text'] ?>
        </a>
    </div>

    <small><?php echo $instance['texto_footer'] ?></small>
</div>
<?php $widget_horizontal = ob_get_clean(); ?>












    <?php

    if ($instance['estructura'] == 'cuadrado') {
        echo $widget_cuadrado;
    }
    elseif ($instance['estructura'] == 'rectangular') {
        echo $widget_horizontal;
    }
