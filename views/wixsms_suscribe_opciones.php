<div class="wrap">

	<h1>Configure facilmente siguiendo el orden.</h1>

	<form action="options.php" id="form-wixsms_suscribe" method="post" autocomplete="off">

		<?php
		settings_fields( 'wixsms_suscribe_settings_pagina' );
		do_settings_sections( 'wixsms_suscribe_settings_pagina' );
		submit_button();
		?>

	</form>

</div>


<script type="text/javascript">
	jQuery( document ).on('keyup', '#form-wixsms_suscribe #wixsms_text_field_0', function(event) {
		event.preventDefault();

		/*
		 * Consulta en AJAX mailchmp para obtener las listas
		 */

		var api_key = jQuery(this).val();
		if (api_key.length > 33) {
			wixsms_suscribe_mailchimp_list(api_key);
		}

	});

	function wixsms_suscribe_mailchimp_list(api_key) {
		var url_accion = "<?php echo admin_url( 'admin-ajax.php' ) ?>";

		/*
		$.post(
			url_accion,
			{
				action: 'contar_visitas_patrocinadas',
				url: $url
			},
			function(data, textStatus, xhr) {
		});
		 */

		console.log(api_key);
	}
</script>
