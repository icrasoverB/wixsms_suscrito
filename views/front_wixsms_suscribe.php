<div class="wixsms_suscribe-modal wixsms_modal-relative" id="wixsms_suscribe-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<button type="button" class="wixsms_close" data-dismiss="wixsms_modal" aria-label="Close" onclick="wixsms_suscribe_visitas_cookie('relative');">
				<span aria-hidden="true">&times;</span>
			</button>

			<!-- body -->
			<div class="wixsms_modal-body">

				<!-- Titulo -->
				<span class="wixsms_suscribe_titulo Galindo"><?php echo $instance['title'] ?></span>

				<?php
					$wixsms_suscribe_descripcion = "";
					switch (wixsms_get('wixsms_suscribe_status')) {
						case 'email_invalido':
							$wixsms_suscribe_descripcion = $instance['error_message'];
							break;

						case 'suscrito':
							$wixsms_suscribe_descripcion = $instance['success_message'];
							break;

						case 'ya_suscrito':
							$wixsms_suscribe_descripcion = $instance['already_subscribed_message'];
							break;

						default:
							$wixsms_suscribe_descripcion = $instance['descripcion'];
							break;
					}
				?>
				<!-- Descripcion -->
				<p class="wixsms_suscribe_descripcion Galindo"><?php echo $wixsms_suscribe_descripcion ?></p>


				<!-- Boton Facebook -->
				<div id="wixsms_suscribe-btn_facebook" class="wixsms_btn_conteiner wixsms_d-block">
					<a href="<?php echo $suscribeUrl ?>" class="wixsms_d-block wixsms_btn Galindo">
						<i class="fa fa-facebook" aria-hidden="true"></i>
						<?php echo $instance['facebook_button_text']  ?>
					</a>
				</div>

				<!-- Boton Email -->
				<div id="wixsms_suscribe-btn_email" class="wixsms_btn_conteiner wixsms_d-block">
					<button type="submit" class="wixsms_d-block wixsms_btn Galindo">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
						<?php echo $instance['email_button_text']  ?>
					</button>
				</div>


				<form  action="<?php echo site_url( '?wixsms_suscribe_email' ); ?>" method="POST" class="wixsms_d-block wixsms_suscribe-form_email wixsms_hidden" role="form">
				   <?php wp_nonce_field( 'wixsms_suscribe_email', 'wixsms_suscribe_nonce_field' ); ?>
					<input name="email" type="email" class="wixsms_form-control Roboto" id="" placeholder="<?php echo $instance['email_placeholder']  ?>">
					<button type="submit" class="wixsms_btn wixsms_btn-send Galindo">
						<?php echo __( 'Métele', 'wixsms_suscribe' ) ?>
						<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
					</button>
				</form>
			</div>
			<!--!body -->

			<!--
			-->
			<div class="wixsms_modal-footer">
				<small class="wixsms_d-block wixsms_text-xs-left">
					<?php echo $instance['texto_footer']  ?>
				</small>
			</div>

		</div>
	</div>
</div>


<div class="wixsms_suscribe_modal-backdrop in wixsms_hidden"></div>

<script type="text/javascript">
	$(document).on('ready', function() {
		<?php if (strtolower(wixsms_get('wixsms_suscribe_status')) === 'suscrito'): ?>
			wixsms_suscribe_relative_to_fixed();
		<?php endif ?>

		<?php if ($instance['mostrar_flotante'] === 'on'): ?>
		if (getCookie('wixsms_suscribe_fixed') === 'fixed') {
				setTimeout( wixsms_suscribe_relative_to_fixed , <?php echo $instance['segundos_ventana'] * 1000  ?> );
		}
		<?php endif ?>
	});

</script>
