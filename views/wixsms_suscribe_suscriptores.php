<div class="wrap">

    <h1>Personas suscritas</h1>
    <div class="wrap" style="margin-bottom: 10px;">
        <form method='get' action="admin.php?page=viewImportarSuscriptores">
            <input type="hidden" name='page' value="ofd_suscribe_suscriptores"/>
            <input type="hidden" name='noheader' value="1"/>
            <input type="radio" name='format' id="formatCSV"  value="csv" checked="checked"/>  <label for"formatCSV">csv</label>
            <input type="radio" name='format' id="formatXLS"  value="xls"/>  <label for"formatXLS">xls</label>
            <input type="radio" name='format' id="formatXLSX" value="xlsx"/> <label for"formatXLSX">xslx</label>
            <input style="display: block; margin-top: 12px;width: 10%;" class="option-tree-ui-button button button-primary" type="submit" name='export' id="csvExport" value="Export"/>
            <input type="hidden" name="fil_exc_email" id="fil_email" value="<?php echo $_GET["fil_email"]; ?>" placeholder="Buscar por email"/>
            <input type="hidden" name="fil_exc_first_name" value="<?php echo $_GET["fil_first_name"]; ?>" placeholder="Buscar por nombre"/>
            <input type="hidden" name="fil_exc_telefono" value="<?php echo $_GET["fil_telefono"]; ?>" placeholder="Buscar por telefono"/>
            <input type="hidden" name="fil_exc_ticket" value="<?php echo $_GET["fil_ticket"]; ?>" placeholder="Buscar por ticket"/>
            <input type="hidden" name="fil_exc_status" value="<?php echo $_GET["fil_status"]; ?>" placeholder="Buscar por ticket"/>

        </form>
    </div>

    <table class="wp-list-table widefat fixed posts">
        <thead>
            <tr>
                <th><?php _e('ID'); ?></th>
                <th><?php _e('Correo Electrónico', 'wixsms'); ?></th>
                <th><?php _e('Nombre y Apellido', 'wixsms'); ?></th>
                <th><?php _e('Like', 'wixsms'); ?></th>
                <th><?php _e('Total sms', 'wixsms'); ?></th>
                <th><?php _e('Fecha Registro', 'wixsms'); ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th><?php _e('ID'); ?></th>
              <th><?php _e('Correo Electrónico', 'wixsms'); ?></th>
              <th><?php _e('Nombre y Apellido', 'wixsms'); ?></th>
              <th><?php _e('Like', 'wixsms'); ?></th>
              <th><?php _e('Total sms', 'wixsms'); ?></th>
              <th><?php _e('Fecha Registro', 'wixsms'); ?></th>
            </tr>
        </tfoot>

        <tbody>
            <?php foreach ($suscriptores as $suscriptor): ?>
                <tr>
                    <td><?php echo $suscriptor->id ?></td>
                    <td><?php echo $suscriptor->email ?></td>
                    <td><?php echo $suscriptor->first_name ?></td>
                    <td><?php echo $suscriptor->like ?></td>
                    <td><?php echo $suscriptor->totalsms ?></td>
                    <td><?php echo $suscriptor->fecha_reg ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>

    </table>
    <?php
    //Limito la busqueda
    $TAMANO_PAGINA = 10;
    //examino la página a mostrar y el inicio del registro a mostrar
    if(isset($_GET["pagina"])){
      $pagina = $_GET["pagina"];
      $inicio = ($pagina - 1) * $TAMANO_PAGINA;
    }
    else {
       $inicio = 0;
       $pagina = 1;
    }

    //calculo el total de páginas
    $total_paginas = ceil($count_suscriptores / $TAMANO_PAGINA);
    ?>


    <div class="paginate">
        <?php
        if ((isset($_GET['paged'])) && ($_GET['paged'] != '')) {
            $current = $_GET['paged'];
        }
        else {
            $current = 0;
        }

        $args = array(
            'base' => '%_%',
            'format' => '?paged=%#%',
            'total' => round($count_suscriptores / 10),
            'current' => $current,
            'show_all' => false,
            'end_size' => 3,
            'mid_size' => 1,
            'prev_next' => true,
            'prev_text' => __('« Anterior'),
            'next_text' => __('Siguiente »'),
            'type' => 'plain',
            'add_args' => false,
            'add_fragment' => '',
            'before_page_number' => '',
            'after_page_number' => ''
        );
        ?>

        <?php echo paginate_links($args); ?>
    </div>

    <ul class="subsubsub">
        <li class="todos">
            <a href="#" class="current">
                <?php _e('Suscritores'); ?>
                <span class="count">(<?php echo $count_suscriptores ?>)</span>
            </a>
        </li>

        <li>
          <a href="#" class="current">
              Total Shared:
              <span class="count shared">0</span>
          </a>
        </li>


    </ul>

</div>



<script type="text/javascript">

jQuery(document).ready(function(){
  jQuery.get('http://graph.facebook.com/?fields=og_object{likes.summary(true).limit(0)},share&id=<?php echo home_url();?>',
  function (data) {
      console.log(data);
      if (data) {
          var share_count = data.share.share_count;
          jQuery('.shared').text(share_count);
      }
  });
});

jQuery(document).ready(function(){
  jQuery.get('https://graph.facebook.com/v2.3/PortalAlCaribe?fields=likes&access_token=EAACEdEose0cBAPt862oDLELH9tfbXdCoFpcrrko1AUJpe7LVYi5pZCHnQzmMVbSpJawq6pyB9Kg2SnkJ5oZBTmXdH0LHStNYoiYsdFyTnYPHLUnQ2ZCmmTVc8pacDywtGmAmJ5Ha0p9AFZBIqPcDPoOzYmCYqIenzd4k7snwKAZDZD',
  function (data) {
      if (data) {
          var like_count = data.likes.toFixed(2);
          jQuery('.like').text(like_count);
      }
  });
});



    jQuery(document).on('click', '.subsubsub a', function (event) {
        event.preventDefault();
        /* Act on the event */
    });
</script>
